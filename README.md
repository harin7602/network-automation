# Getting Started in Building Network Automation Solutions
# Sept 2017
# Assignment-1

Ansible Enviornment is installed in "nms" machine and it is assigned private_network IP=172.16.1.11
Vagrant provider is VMware Fusion and VIRL also installed on VMware Fusion.
Vagrant File is also uploaded to the repository.

Test enviornment is built on Cisco VIRL 1.3 with One Router and One Nexus Switch. 
R1  - Flat-1 : 172.16.1.100
SW1 - Flat-2 : 172.16.1.101 

nms machine I can log into R1 and SW1 and also execute ansible scripts shared in 
https://github.com/ipspace/NetOpsWorkshop.git



